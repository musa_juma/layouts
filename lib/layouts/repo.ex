defmodule Layouts.Repo do
  use Ecto.Repo,
    otp_app: :layouts,
    adapter: Ecto.Adapters.Postgres
end
