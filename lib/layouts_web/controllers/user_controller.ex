defmodule LayoutsWeb.UserController do
  use LayoutsWeb, :controller

  def index(conn, _params) do
    render(conn, "user.html")
  end

end
