defmodule LayoutsWeb.DashboardController do
  use LayoutsWeb, :controller

  def index(conn, _params) do
    render(conn, "dashboard.html")
  end
end
