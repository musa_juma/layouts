defmodule LayoutsWeb.Router do
  use LayoutsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LayoutsWeb do
    pipe_through :browser

    get "/", PageController, :index

    get "/dashboard", DashboardController, :index

    get "/user", UserController, :index

  end

  # Other scopes may use custom stacks.
  # scope "/api", LayoutsWeb do
  #   pipe_through :api
  # end
end
