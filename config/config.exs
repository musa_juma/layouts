# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :layouts,
  ecto_repos: [Layouts.Repo]

# Configures the endpoint
config :layouts, LayoutsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8jCjdfoYM5HGG/9XPTxe4qVahR9D4zQST5ZdSNoUenX5zr6nMPySF14KQvYB6sXv",
  render_errors: [view: LayoutsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Layouts.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
